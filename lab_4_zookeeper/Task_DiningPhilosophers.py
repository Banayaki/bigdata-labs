import logging

from time import time, sleep
from multiprocessing import Process
logging.basicConfig()

from kazoo.client import KazooClient
from icecream import ic


class Philosopher(Process):
    def __init__(self, root: str, _id: int, fork_path: str, duration: int = 100):
        super().__init__()
        self.url = f'{root}/{_id}'
        self.root = root
        self.fork = fork_path
        self.id = _id
        self.left_fork_id = _id
        self.right_fork_id = _id + 1 if _id + 1 < 5 else 0
        self.duration = duration

        self.counter = 0

    def run(self):
        zk = KazooClient()
        zk.start()

        table_lock = zk.Lock(f'{self.root}/table', self.id)
        left_fork = zk.Lock(f'{self.root}/{self.fork}/{self.left_fork_id}', self.id)
        right_fork = zk.Lock(f'{self.root}/{self.fork}/{self.right_fork_id}', self.id)

        start = time()
        while time() - start < self.duration:
            print(f'Philosopher {self.id}', 'Im thinking')
            with table_lock:
                if len(left_fork.contenders()) == 0 and len(right_fork.contenders()) == 0:
                    left_fork.acquire()
                    right_fork.acquire()
                    
            if left_fork.is_acquired:
                print(f'Philosopher {self.id}', 'Im eating')
                self.counter += 1
                sleep(1)
                left_fork.release()
                right_fork.release()
            sleep(0.5)
            
        ic(self.id, self.counter)
        zk.stop()
        zk.close()


if __name__ == "__main__":
    master_zk = KazooClient()
    master_zk.start()

    if master_zk.exists('/task1'):
        master_zk.delete('/task1', recursive=True)

    master_zk.create('/task1')
    master_zk.create('/task1/table')
    master_zk.create('/task1/forks')
    master_zk.create('/task1/forks/1')
    master_zk.create('/task1/forks/2')
    master_zk.create('/task1/forks/3')
    master_zk.create('/task1/forks/4')
    master_zk.create('/task1/forks/5')

    root = '/task1'
    fork_path = 'forks'
    for i in range(5):
        p = Philosopher(root, i, fork_path, 20)
        p.start()
