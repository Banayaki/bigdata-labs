import logging
import threading
from multiprocessing import Process
from time import sleep, time

import numpy as np
from kazoo.protocol.paths import join

logging.basicConfig()

from icecream import ic
from kazoo.client import KazooClient


class Client(Process):
    def __init__(self, root: str, _id: int):
        super().__init__()
        self.url = f'{root}/{_id}'
        self.root = root
        self.id = _id

    def run(self):
        zk = KazooClient()
        zk.start()

        value = b'commit' if np.random.random() > 0.5 else b'abort'
        print(f'Client {self.id} request {value.decode()}')
        zk.create(self.url, value, ephemeral=True)
        
        @zk.DataWatch(self.url)
        def watch_myself(data, stat):
            if stat.version != 0:
                print(f'Client {self.id} do {data.decode()}')

        sleep(5)

        zk.stop()
        zk.close()


class Coordinator():
    def main(self):
        coordinator = KazooClient()
        coordinator.start()

        if coordinator.exists('/task2'):
            coordinator.delete('/task2', recursive=True)

        coordinator.create('/task2')
        coordinator.create('/task2/tx')

        number_of_clients = 5
        duration = 20
        self.timer = None

        def check_clients():
            clients = coordinator.get_children('/task2/tx')
            commit_counter = 0
            abort_counter = 0
            for client in clients:
                commit_counter += int(coordinator.get(f'/task2/tx/{client}')[0] == b'commit')
                abort_counter += int(coordinator.get(f'/task2/tx/{client}')[0] == b'abort')

            target = b'commit' if commit_counter > abort_counter else b'abort'
            for client in clients:
                coordinator.set(f'/task2/tx/{client}', target)

        @coordinator.ChildrenWatch('/task2/tx')
        def watch_clients(clients):
            if len(clients) == 0:
                if self.timer is not None:
                    self.timer.cancel()
            else:
                if self.timer is not None:
                    self.timer.cancel()

                timer = threading.Timer(duration, check_clients)
                timer.daemon = True
                timer.start()

            if len(clients) < number_of_clients:
                ic('Waiting for the others.', clients)
            elif len(clients) == number_of_clients:
                ic('Check clients')
                timer.cancel()
                check_clients()

        root = '/task2/tx'
        for i in range(5):
            p = Client(root, i)
            p.start()


if __name__ == "__main__":
    Coordinator().main()
