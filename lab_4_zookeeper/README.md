# Prerequsists 
Run zookeeper inside docker container using the following command:
`docker-compose down; docker-compose up --build`

## Connecting via shell
`./bin/zkCli.sh -server localhost:2181`

# Motivation 
In this work I'm trying to solve tasks using python __kazoo__ library.
It's quite similar to the suggested __scala__ library.

# Work
The first part (_before project example_) of the work and the second (_project example_) are done in 
`FirstPart.ipynb` and `SecondPart.py` files respectively. 

# Assignments
Two assigments are done using __kazoo__ and presented in separate files:
- `Task_DiningPhilosophers.py`
- `Task_TwoPhaseCommit.py`