# Lab №1 - Apache Spark Introduction

# Mukhin Artem 6133-010402D

# Summary
In this work I have used Apache Spark (_pyspark_) in python.

I do not provide ``.ipynb`` file with first part of the laboratory due to I worked in pyspark shell 
without jupyter.

``init.sh`` - puts some files in HDFS; installs pip and jupyter

``run_pysparkshell_jupyter.sh`` - runs jupyter notebook that already attached to spark-shell, it means
that Spark Context will be already initialized and HDFS should work.

